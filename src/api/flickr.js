import fetch from 'isomorphic-fetch'

export function photos(url) {
  return fetch(url)
    .then(r => r.json())
    .then(r => r.photoset)
    .then(r => r.photo)
}

const flickr = {
  photos
}

export default flickr
