import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { provideHooks } from 'redial'
import filter from 'lodash/filter'

import LightBox from './LightBox'

import styles from './Photo.scss'

import $ from 'jquery'
import { Freewall } from 'freewall'

import { fetchPhotos, showPhoto } from '../modules/photos'

import { photosSelector, photoSelector } from '../selectors'

const hooks = {
  fetch: ({ dispatch, params }) => { dispatch(fetchPhotos(params.category)) }
}

const mapStateToProps = (state, props) => {
  return {
    photos: photosSelector(state, props),
    photo: photoSelector(state, props),
    category: props.params.category
  }
}

class HomePage extends Component {
  static propTypes = {
    photos: PropTypes.array.isRequired
  }

  componentDidMount() {
    window.addEventListener('resize', () => {
      this.setState({time:(new Date())})
    });
  }

  handlePhotoClick = (e, photoId) => {
    e.preventDefault()
    const { dispatch } = this.props
    dispatch(showPhoto(photoId))
  }

  render() {
    const { photos, category, photo } = this.props

    const columns = []

    columns.push([])

    if (window.innerWidth >= 400) {
      columns.push([])      
    }

    if (window.innerWidth >= 800) {
      columns.push([])      
    }

    if (window.innerWidth >= 1000) {
      columns.push([])      
    }

    if (window.innerWidth >= 1200) {
      columns.push([])      
    }
    
    var counter = 0

    photos.forEach((p, i) => {
      const element = (
        <a key={i} href={`/photos/${p.id}`} data-id={p.id} onClick={(e) => this.handlePhotoClick(e, p.id)}>
          <img className={styles.photo} width={p.width_m} height={p.height_m} src={p.url_m} />
        </a>
      )
      if (counter == columns.length) counter = 0
      columns[counter].push(element)
      counter++
    })

    const photosElements = (
      <div className={styles.columns}>
        <div className={styles.column}>{columns[0]}</div>
        <div className={styles.column}>{columns[1]}</div>
        <div className={styles.column}>{columns[2]}</div>
        <div className={styles.column}>{columns[3]}</div>
        <div className={styles.column}>{columns[4]}</div>
      </div>
    )
    
    var renderLightBox = photo !== 0 && photo !== undefined 
    if (renderLightBox) {
      return (<LightBox photo={photo} />)
    } else {
      return (
        <div>
          <div className={styles.title}>Blaisé Clicks</div>
          <div>
            <ul className={styles.menu}>
              <li className={`${category === undefined ? styles.active : ''}`}>
                <Link to="/">All</Link>
              </li>
              <li className={`${category === "music" ? styles.active : ''}`}>
                <Link to="/music">Music</Link>
              </li>
              <li className={`${category === "people" ? styles.active : ''}`}>
                <Link to="/people">People</Link>
              </li>
              <li className={`${category === "travel" ? styles.active : ''}`}>
                <Link to="/travel">Travel</Link>
              </li>
              <li>
                <a href="mailto:hello@blaiseclicks.com">Contact</a>
              </li>
            </ul>
          </div>
          <div className={styles.photos}>
            {photosElements}
          </div>
        </div>
      )
    }
  }
}

const HomePageWithHooks = provideHooks(hooks)(HomePage)
export default connect(mapStateToProps)(HomePageWithHooks)
