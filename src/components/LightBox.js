import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'
import styles from './Lightbox.scss'
import { showPhoto } from '../modules/photos'

class Lightbox extends Component {
  static propTypes = {
    photo: PropTypes.object.isRequired
  }

  handleCloseWindow = (e) => {
    const { dispatch } = this.props
    dispatch(showPhoto(0))

    // const previousLocation = window.previousLocation
    // if (previousLocation !== undefined && previousLocation.pathname === "/") {
    //   e.preventDefault()
    //   browserHistory.goBack()
    // }
  }

  render() {
    const { photo } = this.props
    var inlineStyles = {
      maxWidth: "100%"
    }
    return (
      <div className={styles.box}>
        <div className={styles.wrapper} onClick={this.handleCloseWindow}>
          <div className={styles.close}>
            <Link to={'/'} onClick={this.handleCloseWindow}>X</Link>
          </div>
          <img style={inlineStyles} className={styles.content} src={photo.url_l} onClick={(e) => e.stopPropagation()} />
        </div>
      </div>
    )
  }
}


export default connect()(Lightbox)