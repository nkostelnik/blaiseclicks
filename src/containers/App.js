import React, { Component, PropTypes } from 'react'

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  }

  componentWillReceiveProps() {
    window.previousLocation = this.props.location
  }

  render() {
    return (
      <div>
        <div>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default App
