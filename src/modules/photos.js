import { createAction, handleActions } from 'redux-actions'
import { call, put, fork } from 'redux-saga/effects'
import { takeEvery } from 'redux-saga'
import flickr from '../api/flickr'
import reverse from 'lodash/reverse'

export const FETCH_PHOTOS = 'PHOTOS/FETCH'
export const ADD_PHOTOS = 'PHOTOS/ADD'
export const SHOW_PHOTO = 'SHOW_PHOTO'

export const fetchPhotos = createAction(FETCH_PHOTOS)
export const addPhotos = createAction(ADD_PHOTOS, (category, photos) => ({[category]: photos}))
export const showPhoto = createAction(SHOW_PHOTO, (photoId) => photoId)

export const actions = { fetchPhotos }

const urls = {
  all: '72157671123011820',
  music: '72157666162819520',
  people: '72157664417346383',
  travel: '72157666636753086',
  nature: '72157664417509193'
}

function * fetchPhotoset(category) {
  var setId = urls[category]
  const url = `https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=1a936b38c156d688f800789ecb19261c&photoset_id=${setId}&user_id=141593409@N08&extras=url_sq%2C+url_t%2C+url_s%2C+url_m%2C+url_o%2C+url_l&format=json&nojsoncallback=1`
  const response = yield call(flickr.photos, url)
  yield put(addPhotos(category, response))
}

export function * performFetchPhotos() {
  yield fetchPhotoset('all')
  yield fetchPhotoset('music')
  yield fetchPhotoset('people')
  yield fetchPhotoset('travel')
}

export default function * saga() {
  yield [
    fork(function* a() { yield* takeEvery(FETCH_PHOTOS, performFetchPhotos) })
  ]
}

export const reducers = handleActions({
  [ADD_PHOTOS]: (state, action) => ({ ...state, ...action.payload })
}, { })


export const lightbox = handleActions({
  [SHOW_PHOTO]: (state, action) => action.payload,
}, 0)