import forOwn from 'lodash/forOwn'
import find from 'lodash/find'
import shuffle from 'lodash/shuffle'

import { createSelector } from 'reselect'

const photos = (state) => state.photos
const category = (state, props) => props.params.category
const photoId = (state, props) => state.lightbox

export const photosSelector = createSelector(
  [photos, category],
  (photos, category) => {
    var allPhotos = []
    if (category === undefined) category = 'all'
    if (category in photos) {
      allPhotos = allPhotos.concat(photos[category])
    }
    return allPhotos
  })

export const photoSelector = createSelector(
  [photosSelector, photoId],
  (photos, photoId) => {
    if (photoId === undefined) return undefined
    const photo = find(photos, p => p.id === photoId)
    return photo
  }
)