import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import { reducers as photos } from '../modules/photos'
import { lightbox as lightbox } from '../modules/photos'
export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    photos,
    lightbox,
    router,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
