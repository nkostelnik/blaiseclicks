import { fork } from 'redux-saga/effects'
import photos from '../modules/photos'

export default function * root() {
  yield [
    fork(photos)
  ]
}
